/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA pass through bare metal project
 * 		File: - "main.c"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [07-11-2018]: Repo changes
 *
 *---------------------------------------------------------------------------*/
#include "main.h"

void checkHW()
{
	xil_printf("- Hw module ready: %u\r\n",XPassthrough_IsReady(&module_hw));
	xil_printf("- Hw module idle: %u \r\n", XPassthrough_IsIdle(&module_hw));
	xil_printf("- Hw module done: %u \r\n", XPassthrough_IsDone(&module_hw));

	xil_printf("- DMA is busy direction dma to device: %u\r\n",
			XAxiDma_Busy(&AxiDma, XAXIDMA_DMA_TO_DEVICE));
	xil_printf("- DMA is busy direction device to dma: %u\r\n",
			XAxiDma_Busy(&AxiDma, XAXIDMA_DEVICE_TO_DMA));
}


int devicesReset(u16 DeviceIdDMA, u16 DeviceIdModule)
{
	xil_printf("-------------------------\r\n");
	xil_printf("Entering devicesReset ...\r\n");

	XAxiDma_Config *CfgPtrDMA;

	int Status = XST_SUCCESS;

	xil_printf("- DMA lookup table configuration ... ");
	CfgPtrDMA = XAxiDma_LookupConfig(DeviceIdDMA);
	if (!CfgPtrDMA) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

	xil_printf("- Initializes the DMA engine ... ");
	Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtrDMA);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

	xil_printf("- Pass_Through hw module initialization ... ");
	Status = XPassthrough_Initialize(&module_hw, DeviceIdModule);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

	xil_printf("Exiting devicesReset ... \r\n");

	return Status;
}


int DMATransfer(u16 DeviceID)
{
	xil_printf("--------------------------------\r\n");
	xil_printf("Entering DMATransfer ... \r\n");

	int Status = XST_SUCCESS;

	u8 *TxBufferPtr __attribute__((aligned(8)));
	TxBufferPtr = (u8*)TX_BUFFER_BASE;

	xil_printf("Filling input data ... ");
	for(int i = 0; i < (NUMBER_PIXELS_INPUT);  i++)
	{
		TxBufferPtr[i] = i;
	}
	xil_printf("OK \r\n");

	xil_printf("DMA Simple transfer ... ");
	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) TxBufferPtr,
			NUMBER_PIXELS_INPUT, XAXIDMA_DMA_TO_DEVICE);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

	xil_printf("Pass Through start ... ");
	XPassthrough_Start(&module_hw);
	xil_printf("OK \r\n");

	while ((XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE))) {
	   	// wait
	}
	xil_printf("Transfer DMA to Device done ! \r\n");

	xil_printf("Checking HW after simple transfer ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

	xil_printf("Exiting DMATransfer ... \r\n");

   	return Status;
}



int DMAReceive(u16 DeviceID)
{
   	xil_printf("--------------------------------\r\n");
   	xil_printf("Entering DMAReceive function \r\n");

	int Status = XST_SUCCESS;
	int c_errors = 0;

	u8 *RxBufferPtr __attribute__((aligned(8)));
	RxBufferPtr = (u8*)RX_BUFFER_BASE;

	u8 value;


	//Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, NUMBER_PIXELS_OUTPUT);

	/* wait until the module finish */
	/*
	while (!XPassthrough_IsDone(&module_hw)) {
		// wait, if non-zero, is done.
		//		 if zero, nothing is done so WAIT until
	}
*/
	//xil_printf("Hw module has finished !\r\n");

	xil_printf("Checking HW before receive transfer ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

   	/* Take data  */
	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
			NUMBER_PIXELS_OUTPUT, XAXIDMA_DEVICE_TO_DMA);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

   	while ((XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA))) {
   		// wait
   	}
	xil_printf("Transfer Device to DMA done !\r\n");

	xil_printf("Checking HW after receive transfer ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

	xil_printf("Checking data received ... ");
   	for (int i=0; i<NUMBER_PIXELS_OUTPUT; i++) {

		value = RxBufferPtr[i];

		if (value!=i) {
			c_errors++;
		}
	}
   	xil_printf("OK \r\n");

   	if (c_errors != 0) {
   		return XST_FAILURE;
   	}
   	else {
   		return XST_SUCCESS;
   	}
}


int main()
{

   	xil_printf("\r\n------------------------- \r\n");
   	xil_printf("Entering main ... \r\n");

    int Status;

   	/*------------- Run the DMAReset function to reset the DMA --------------*/
   	Status = devicesReset(DMA_DEV_ID, MODULE_HW_ID);

   	if (Status != XST_SUCCESS) {
   		xil_printf("devicesReset Failed\r\n");
  		return XST_FAILURE;
   	}


   	/*---------------------- Send data to module ----------------------------*/
   	Status = DMATransfer(DMA_DEV_ID);

   	if (Status != XST_SUCCESS) {
		xil_printf("DMATrasnfer Failed\r\n");
		return XST_FAILURE;
   	}

   	/*------------------------ Receive data ---------------------------------*/
   	Status = DMAReceive(DMA_DEV_ID);

   	if (Status != XST_SUCCESS) {
		xil_printf("DMATrasnfer Failed\r\n");
		return XST_FAILURE;
   	}

   	xil_printf("Successfully ran dma_test Example\r\n");
   	xil_printf("--- Exiting main() --- \r\n");

    cleanup_platform();

   	return XST_SUCCESS;
}
