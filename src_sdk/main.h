/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA pass through bare metal project
 * 		File: - "module.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [07-11-2018]: Repo changes
 *
 *---------------------------------------------------------------------------*/
#ifndef MAIN_H_
#define MAIN_H_

/*---------------------------- Include Files --------------------------------*/
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include "xaxidma.h"
#include "xparameters.h"
#include "xdebug.h"
#include "xpassthrough.h"


/*------------------------- Constant Definitions ----------------------------*/
#define DMA_DEV_ID				XPAR_AXIDMA_0_DEVICE_ID
#define MODULE_HW_ID			XPAR_PASSTHROUGH_0_DEVICE_ID


/*
 * Device hardware build related constants.
 */

#define MEM_BASE_ADDR		0x00000000

#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x20000000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x25FFFFFF)


/*------------------------- Variable Definitions ----------------------------*/
/*
 * Device instance definitions
 */
XAxiDma AxiDma;
XPassthrough module_hw;

#define NUMBER_PIXELS_INPUT  (10)
#define NUMBER_PIXELS_OUTPUT (10)

/*------------------------- Function Prototypes -----------------------------*/
int devicesReset(u16 DeviceIdDMA, u16 DeviceIdModule);
int DMATransfer(u16 DeviceId);
int DMAReceive(u16 DeviceId);


#endif /* MAIN_H_ */
