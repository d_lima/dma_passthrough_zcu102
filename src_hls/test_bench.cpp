/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA pass through bare metal project
 * 		File: - "test_bench.c"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [07-11-2018]: Repo changes
 *
 *---------------------------------------------------------------------------*/
#include "test_bench.h"

using namespace std;


/*------------------------------- Main --------------------------------------*/
int main(int argc, char** argv )
{
    /* Errors counter */
    int c_errors = 0;

    /* Structures with data*/
    struct_data input_data_structs[10];
    struct_data dest_data[10];

    for (int i=0; i<10; i++)
    {
    	input_data_structs[i].data = i;
    	input_data_structs[i].tlast = false;

    	// Last value true
    	if (i==9){
    		input_data_structs[i].tlast = true;
    	}
    }

    printf("----------------------\n");
    printf("Executing Hardware implementation ...\n");

    /* Call hardware */
    printf("Calling the UUT ...");
    passThrough(input_data_structs, dest_data);
    printf("OK\n");

    printf("----------------------\n");
    printf("Checking result ...\n");

    /* Loop to check bytes */
    for (int i=0; i<(10); i++)
    {
        /* Check if the values are different */
        if (dest_data[i].data != i)
        {
            c_errors++;
            printf("i: %u input: %u,\n", i, dest_data[i].data);
        }
    }
    printf("OK\n");

    printf("Checking simulation result... \n");
    if (c_errors!=0) {
        fprintf(stdout, "FAIL !\n");
        fprintf(stdout, "Number of errors: %d \n", c_errors);
        return 1;
    }
    else {
        fprintf(stdout, "PASS !\n");
	    return 0;
    }
}


