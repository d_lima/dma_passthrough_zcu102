/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA pass through bare metal project
 * 		File: - "module.c"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [07-11-2018]: Repo changes
 *
 *---------------------------------------------------------------------------*/
#include "module.h"


boolean passThrough(struct_data input[10], struct_data output[10])
{
#pragma HLS INTERFACE axis port=output
#pragma HLS INTERFACE axis port=input
#pragma HLS INTERFACE s_axilite port=return

	int k;

	byte temp_data[10];
	bool temp_tlast[10];

	for (k=0; k<10; k++)
	{
		temp_data[k] = input[k].data;
		temp_tlast[k] = input[k].tlast;
	}

    for(k=0;k<(10);k++)
    {
        output[k].data = temp_data[k];
        output[k].tlast = temp_tlast[k];
    }

    return true;
}
