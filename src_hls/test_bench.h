/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA pass through bare metal project
 * 		File: - "test_bench.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [07-11-2018]: Repo changes
 *
 *---------------------------------------------------------------------------*/
#ifndef TB_H
#define TB_H


/*----------------------------- Libraries -----------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

/*---------------------------- Project headers ------------------------------*/
#include "module.h"


#endif // STRETCH4X_TB_H
