/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA pass through bare metal project
 * 		File: - "module.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [07-11-2018]: Repo changes
 *
 *---------------------------------------------------------------------------*/
#ifndef MODULE_HW_H
#define MODULE_HW_H


/*------------------------- Type definitions --------------------------------*/
typedef bool boolean;

typedef unsigned char byte;

/*------------------------- Constant Definitions ----------------------------*/

struct struct_data {
	byte data;
	bool tlast;
};

/*----------------------- Functions prototypes ------------------------------*/
boolean passThrough(struct_data input[10], struct_data output[10]);


#endif // MODULE_HW_H
